#include "Coin.hpp"


void Coin::_register_methods() {

	register_method("_ready", &Coin::_ready);
	register_method("_init", &Coin::_init);
	register_method("_process", &Coin::_process);
	register_method("pick_up", &Coin::pick_up);
	register_method("_on_Coin_area_entered", &Coin::_on_Coin_area_entered);
	register_method("_on_Timer_timeout", &Coin::_on_Timer_timeout);
	register_method("_on_Tween_tween_completed", &Coin::_on_Tween_tween_completed);
}

void Coin::_init() {

}

void Coin::_ready() {
	animation = Object::cast_to<AnimatedSprite>(this->find_node("AnimatedSprite"));
	tween = Object::cast_to<Tween>(this->find_node("Tween"));
	timer = Object::cast_to<Timer>(this->find_node("Timer"));
	this->add_to_group("coins");
	int rand_rage = rand() % 5 + 3;
	timer->set_wait_time(rand_rage);
	timer->start();
	tween->interpolate_property(animation, "scale", animation->get_scale(),
								animation->get_scale() * 3, 0.3,
								Tween::TransitionType::TRANS_QUAD,
								Tween::EASE_IN_OUT);
	tween->interpolate_property(animation, "modulate", Color(1, 1, 1, 1),
								Color(1, 1, 1, 0), 0.3,
								Tween::TransitionType::TRANS_QUAD,
								Tween::EASE_IN_OUT);
}

void Coin::_on_Timer_timeout() {
	animation->set_frame(0);
	animation->play();
}

void Coin::_process(float delta) {

}

void Coin::_on_Tween_tween_completed(Object* o, NodePath key) {
	queue_free();
}



void Coin::pick_up() {
	set_monitoring(false);
	tween->start();
}

void Coin::_on_Coin_area_entered(Area2D* area ) {
	if (area->is_in_group("obstacles")) {
		int x = screen_size.x;
		int y = screen_size.y;
		set_position(Vector2(rand() % x, rand() % y));
	}

}

void Coin::set_screen_size(Vector2 screen_size) {
	this->screen_size = screen_size;
}
