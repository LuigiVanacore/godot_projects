#include "Hud.hpp"


void Hud::_register_methods() {

	
	register_method("_ready", &Hud::_ready);
	register_method("_process", &Hud::_process);
	register_method("_on_MessageTimer_timeout", &Hud::_on_MessageTimer_timeout);
	register_method("_on_StartButton_pressed", &Hud::_on_StartButton_pressed);
	register_method("show_game_over", &Hud::show_game_over);
	register_method("show_start_message", &Hud::show_start_message);


	register_signal<Hud>((char*)"start_game", "node", GODOT_VARIANT_TYPE_OBJECT);
}


void Hud::_init() {

}


void Hud::_ready() {
	message_label = Object::cast_to<Label>(this->find_node("MessageLabel"));
	start_button = Object::cast_to<Button>(this->find_node("StartButton"));

	score_label = Object::cast_to<Label>(this->find_node("ScoreLabel"));
	if (score_label == nullptr) {
		Godot::print("error");
	}
	time_label = Object::cast_to<Label>(this->find_node("TimeLabel"));
	if (time_label == nullptr) {
		Godot::print("error");
	}

	message_timer = Object::cast_to<Timer>(this->find_node("MessageTimer"));
	message_timer->stop();
}

void Hud::_process(float delta) {

	}


void Hud::update_score(int value) {
	score_label->set_text(godot::String::num_int64(value));
}

void Hud::update_time(int value) {
	time_label->set_text(godot::String::num_int64(value));

}


void Hud::_on_MessageTimer_timeout() {
	show_start_message();
}

void Hud::_on_StartButton_pressed() {
	start_button->hide();
	message_label->hide();
	emit_signal("start_game", this);
}

void Hud::show_message(godot::String message) {
	message_label->set_text(message);
	message_label->show();

	message_timer->start();
}

void Hud::show_game_over() {
	show_message("Game Over");
	std::thread thread_show_message(&Hud::show_start_message, this);
	thread_show_message.detach();
	//std::thread thread_show_message(&Hud::show_start_message,this);
	//godot::Thread* thread_show_message = Thread::_new();
	//thread_show_message->start(Hud::_new(), "show_start_message");

}

void Hud::show_start_message() {
	do {
		std::this_thread::yield();
	} while (!message_timer->is_stopped());
	start_button->show();
	message_label->set_text("Coin Rush!");
	message_label->show();
}
 

