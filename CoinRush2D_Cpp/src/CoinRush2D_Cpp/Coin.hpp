#pragma once
#include <Godot.hpp>
#include <Area2D.hpp>
#include <AnimatedSprite.hpp>
#include <Tween.hpp>
#include <Timer.hpp>


using namespace godot;

class Coin : public Area2D {
	GODOT_CLASS(Coin, Area2D)

private:
	Vector2 screen_size;
	AnimatedSprite* animation;
	Tween* tween;
	Timer* timer;

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _process(float);
	void pick_up();
	void _on_Timer_timeout();
	void _on_Tween_tween_completed(Object*, NodePath);
	void _on_Coin_area_entered(Area2D*);
	void set_screen_size(Vector2);
};