#pragma once
#include <Godot.hpp>
#include <Node2D.hpp>
#include <PackedScene.hpp>
#include <Position2D.hpp>
#include <Timer.hpp>
#include <Node2D.hpp>
#include <stdlib.h> 
#include <Array.hpp>
#include <ResourceLoader.hpp>
#include <AudioStreamPlayer.hpp>
#include "Player.hpp"
#include "Hud.hpp"
#include "Coin.hpp"



namespace godot {


	class Main : public Node2D {
		GODOT_CLASS(Main, Node2D)

	private:
		ResourceLoader* loader;
		godot::Ref<PackedScene> coin;
		int playtime;
		int level;
		int score;
		int time_left;
		Vector2* screensize;
		Player* player;
		Area2D* cactus;
		Position2D* player_start;
		Timer* game_timer;
		Hud* hud;
		Node2D* coin_container;
		AudioStreamPlayer* coin_sound;
		AudioStreamPlayer* level_sound;
		AudioStreamPlayer* end_sound;
		bool playing = false;

	public:
		static void _register_methods();

		void _init();
		void _ready();
		void _process(float delta);
		void new_game();
		void spawn_coins();
		void game_over();
		void _on_GameTimer_timeout();
		void _on_Player_hurt();
		void _on_Player_pickup(Node*);
		void _on_Hud_start_game();
	};

}