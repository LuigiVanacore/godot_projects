#include "Main.hpp"

using namespace godot;

void Main::_register_methods() {
    register_property<Main, int>("playtime", &Main::playtime, 3);
   
    //register_property<Main, PackedScene*>("Coin", &Main::coin, PackedScene::_new());
    
    register_method("_ready", &Main::_init);
    register_method("_ready", &Main::_ready);
    register_method("_process", &Main::_process);
    register_method("_on_GameTimer_timeout", &Main::_on_GameTimer_timeout);
    register_method("_on_Player_hurt", &Main::_on_Player_hurt);
    register_method("_on_Player_pickup", &Main::_on_Player_pickup);
    register_method("_on_Hud_start_game", &Main::_on_Hud_start_game);

    
}

void Main::_init() {

}

void Main::_ready() {
    loader = ResourceLoader::get_singleton();
    coin = loader->load("res://Coin.tscn");
    screensize = new Vector2(this->get_viewport_rect().size);
    player = Object::cast_to<Player>(this->get_node("Player"));
    player_start = Object::cast_to<Position2D>(this->get_node("PlayerStart"));
    hud = Object::cast_to<Hud>(this->get_node("Hud"));
    coin_container = Object::cast_to<Node2D>(this->get_node("CoinContainer"));
    game_timer = Object::cast_to<Timer>(this->get_node("GameTimer"));
    coin_sound = Object::cast_to<AudioStreamPlayer>(this->get_node("CoinSound"));
    level_sound = Object::cast_to<AudioStreamPlayer>(this->get_node("LevelSound"));
    end_sound = Object::cast_to<AudioStreamPlayer>(this->get_node("EndSound"));
    cactus = Object::cast_to<Area2D>(this->get_node("Cactus"));


    player->set_screen_size(*screensize);
    player->hide();
    cactus->hide();
    Object::cast_to<Area2D>(this->get_node("Cactus"))->hide();
    player->connect("pickup", this, "_on_Player_pickup");
    player->connect("hurt", this, "_on_Player_hurt");
}


void Main::new_game() {
    playing = true;
    level = 1;
    score = 0;
    time_left = 15;
    player->set_player_start(player_start->get_position());
    player->show();
    player->set_process(true);
    cactus->show();
    game_timer->start();
    hud->update_score(score);
    hud->update_time(time_left);
    spawn_coins();
   
}

void Main::spawn_coins() {
    level_sound->play();
    for (int i = 0; i < level + 4; i++) {
       
        Coin* c = Object::cast_to<Coin>(coin->instance());
        c->set_screen_size(*screensize);
        int x = screensize->x;
        int y = screensize->y;
        c->set_position(Vector2(rand() %  x, rand() % y));
        coin_container->add_child(c);
    }
}


void Main::_process(float delta) {
    if (playing && (coin_container->get_child_count() == 0)) {
        level += 1;
        time_left += 5;
        spawn_coins();
    }
}


void Main::game_over() {
  
    end_sound->play();
    playing = false;
    game_timer->stop();
    int coin_count = coin_container->get_child_count();
    for (int i = 0; i < coin_count; i++) {
        Coin* c = Object::cast_to<Coin>(coin_container->get_child(i));
        c->queue_free();
    }
    hud->show_game_over();
    player->die();
}

void Main::_on_GameTimer_timeout() {

    time_left -= 1;
    hud->update_time(time_left);
    if (time_left <= 0) {
        game_over();
    }
}

void Main::_on_Player_hurt() {
    game_over();
}

void Main::_on_Player_pickup(Node* n) {
    Godot::print(n->get_name());
    if ( n->is_in_group("coins")) {
        score += 1;
        hud->update_score(score);
        coin_sound->play();
    }
      
}

void Main::_on_Hud_start_game() {
    new_game();
}
