#pragma once
#include <Godot.hpp>
#include <CanvasLayer.hpp>
#include <Label.hpp>
#include <Button.hpp>
#include <Timer.hpp>
#include <thread>
#include <Thread.hpp>


using namespace godot;


class Hud : public CanvasLayer {
	GODOT_CLASS(Hud, CanvasLayer)

private:
	Label* message_label;
	Label* score_label;
	Label* time_label;
	Button* start_button;
	Timer* message_timer;

public:
	static void _register_methods();

	void _init();
	void _ready();
	void _process(float delta);
	void update_score(int);
	void update_time(int);
	void show_message(godot::String);
	void show_game_over();
	void show_start_message();
	void _on_MessageTimer_timeout();
	void _on_StartButton_pressed();

};