#include "Player.hpp"




void Player::_register_methods() {
    register_method("_ready", &Player::_ready);
    register_method("_process", &Player::_process);
    register_method("_on_Player_area_entered", &Player::_on_Player_area_entered);

    

    register_signal<Player>((char*)"pickup", "node", GODOT_VARIANT_TYPE_OBJECT);
    register_signal<Player>((char*)"hurt", "node", GODOT_VARIANT_TYPE_OBJECT);

}

void Player::_init() {
   
}

void Player::_ready() {
    speed = 350;
    i = Input::get_singleton();
    animation = static_cast<AnimatedSprite*>(this->find_node("AnimatedSprite"));
    
    start(Vector2(50, 50));
}




void Player::_process(float delta) {
    get_input();
    this->set_position(this->get_position() + velocity * delta);
    this->set_position(
                        Vector2(
                          std::clamp((float)this->get_position().x,(float) 0, (float)screen_size.x),
                          std::clamp((float)this->get_position().y, (float)0, (float)screen_size.y)
                              )
                      );

    set_animation();
    velocity.x = 0;
    velocity.y = 0;
}


void Player::get_input() {
    if (i->is_action_pressed("ui_left")) {
        velocity.x -= 1;
    }
    if (i->is_action_pressed("ui_right")) {
        velocity.x += 1.0;
    }
    if (i->is_action_pressed("ui_up")) {
        velocity.y -= 1;
    }
    if (i->is_action_pressed("ui_down")) {
        velocity.y += 1;
    }
    if (velocity.length() > 0) {
        velocity = velocity.normalized() * speed;
    }
}


void Player::set_animation() {
    if (velocity.length() > 0) {
        animation->set_animation("run");
        animation->set_flip_h(velocity.x < 0);
    }
    else {
        animation->set_animation("idle");
    }
}

void Player::set_player_start(Vector2 position) {
    this->set_position(position);
}

void Player::set_screen_size(Vector2 screen_size) {
    this->screen_size = screen_size;
}


void Player::start(Vector2 start_position) {
    set_process(true);
    set_position(start_position);
    animation->set_animation("idle");
    animation->_set_playing(true);
}

void Player::die() {
    animation->set_animation("hurt");
    set_process(false);
}

void Player::_on_Player_area_entered(Node* area) {
    //if (area->is_in_group("coins")) {
    if (area->is_in_group("coins")) {
        Coin* c = Object::cast_to<Coin>(area);
        c->pick_up();
        emit_signal("pickup", c);
    }
    if (area->is_in_group("obstacles")) {
        emit_signal("hurt");
        die();
    }


}
