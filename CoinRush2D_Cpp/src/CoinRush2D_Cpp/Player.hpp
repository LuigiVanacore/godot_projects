#pragma once
#include <Godot.hpp>
#include <Area2D.hpp>
#include <Input.hpp>
#include <algorithm>
#include <String.hpp>
#include <AnimatedSprite.hpp>
#include "Coin.hpp"
#include <Array.hpp>


using namespace godot;


	class Player : public Area2D {
		GODOT_CLASS(Player, Area2D)

	private:
		Vector2 velocity;
		Vector2 screen_size;
		Input* i;
		AnimatedSprite* animation;
		int speed;



	public:
		static void _register_methods();

		void _init();
		void _ready();
		void _process(float delta);
		void get_input();
		void set_animation();
		void set_player_start(Vector2);
		void set_screen_size(Vector2);
		void start(Vector2 start_position);
		void die();
		void _on_Player_area_entered(Node*);
	};

